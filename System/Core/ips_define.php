<?php

if (!function_exists('ips_define')) {

    function ips_define($name, $value = false) {
        if ($value) {
            if (!isset($_SESSION["IPS"]["PARSER"]["DEFINE"][$name])) {
                return $_SESSION["IPS"]["PARSER"]["DEFINE"][$name] = $value;
            }
            ips_error("Constante {$name} já está definida!");
        }
        return (isset($_SESSION["IPS"]["PARSER"]["DEFINE"][$name]) ? $_SESSION["IPS"]["PARSER"]["DEFINE"][$name] : false);
    }

}
?>