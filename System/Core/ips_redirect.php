<?php

if (!function_exists('ips_redirect')) {

    function ips_redirect($location) {
        header("Location: {$location}");
        ips_halt();
    }

}
?>