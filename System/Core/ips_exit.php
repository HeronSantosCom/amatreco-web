<?php

if (!function_exists('ips_exit')) {

    function ips_exit($print, $level = false) {
        switch ($level) {
            case "ALL":
                if (isset($_SESSION["IPS"])) {
                    unset($_SESSION["IPS"]);
                }
                session_destroy();
                break;
            case "GLOBALS":
                if (isset($_SESSION["IPS"]["GLOBAL"])) {
                    unset($_SESSION["IPS"]["GLOBAL"]);
                }
            default:
                if (isset($_SESSION["IPS"]["PARSER"])) {
                    unset($_SESSION["IPS"]["PARSER"]);
                }
                break;
        }
        exit($print);
    }

}
?>