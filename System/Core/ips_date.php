<?php

if (!function_exists('ips_date')) {

    function ips_date($date, $in = "dd/mm/yyyy", $out = "yyyy-mm-dd") {
        $in = strtoupper($in);
        $out = strtoupper($out);
        $day = substr($date, strpos($in, "DD"), 2);
        $return = str_replace("DD", $day, $out);
        $month = substr($date, strpos($in, "MM"), 2);
        $return = str_replace("MM", $month, $return);
        $year = substr($date, strpos($in, "YYYY"), 4);
        $return = str_replace("YYYY", $year, $return);
        if (strlen($in) == 8) {
            $year = substr($date, strpos($in, "YY"), 2);
            $return = str_replace("YY", $year, $return);
        }
        return $return;
    }

}
?>