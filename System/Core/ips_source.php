<?php

if (!function_exists('ips_source')) {

    function ips_source($source) {
        if (preg_match('/{%*([^%}]+?)(\1|%})/i', $source, $struction)) {
            if (isset($struction[1])) {
                if (preg_match('/(.*?)[\(](.*)[\)](.*)\s/i', $struction[1], $function)) {
                    if (isset($function[2])) {
                        switch (trim($function[1])) {
                            case "get":
                                $value = ips_get(trim($function[2]));
                                break;
                            case "define":
                                $value = ips_define(trim($function[2]));
                                break;
                            case "global":
                                $value = ips_global(trim($function[2]));
                                break;
                            case "path":
                                $cache = true;
                                if (isset($function[3])) {
                                    if (trim($function[3]) == "nocache") {
                                        $cache = false;
                                    }
                                }
                                $value = ips_path(trim($function[2]), $cache);
                                break;
                            case "if":
                                $source = ips_if($function, $source);
                                break;
                            case "for":
                                $source = ips_for($function, $source);
                                break;
                        }
                    }
                }
                $source = ips_source(str_replace($struction[0], (isset($value) ? $value : false), $source));
            }
        }
        return $source;
    }

}
?>