<?php

if (!function_exists('ips_global')) {

    function ips_global($name, $value = false) {
        if ($value) {
            return $_SESSION["IPS"]["GLOBAL"][$name] = $value;
        }
        return (isset($_SESSION["IPS"]["GLOBAL"][$name]) ? $_SESSION["IPS"]["GLOBAL"][$name] : false);
    }

}
?>