<?php

if (!function_exists('ips_if')) {

    function ips_if($condition, $haystack) {
        $function = $condition[0];
        $name = trim($condition[3]);
        $condition = trim($condition[2]);
        $init = strpos($haystack, "{%{$function}%}");
        $parser = substr($haystack, $init);
        $else = strpos($parser, "{% else:{$name} %}");
        $end = strpos($parser, "{% endif:{$name} %}");
        $parser = substr($parser, 0, $end + strlen("{% endif:{$name} %}"));
        $parser_1 = substr($parser, strlen("{%{$function}%}"), $end);
        $parser_2 = null;
        if (strlen($else) > 0) {
            $parser_1 = substr($parser, strlen("{%{$function}%}"), $else - strlen("{%{$function}%}"));
            $parser_2 = substr($parser, $else + strlen("{% else:{$name} %}"), $end - ($else + strlen("{% else:{$name} %}")));
        }
        if (preg_match("/(!=|==|>>|>=|<<|<=)/i", $condition, $function)) {
            if (isset($function[1])) {
                $values = explode($function[1], $condition);
                if (isset($values[1])) {
                    $x = trim($values[0]);
                    $decision = trim($function[1]);
                    $y = trim($values[1]);
                    $offset = false;
                    switch ($decision) {
                        case '==':
                            if ($x == $y) {
                                $offset = true;
                            }
                            break;
                        case '!=':
                            if ($x != $y) {
                                $offset = true;
                            }
                            break;
                        case '>=':
                            if ((double) $x >= (double) $y) {
                                $offset = true;
                            }
                            break;
                        case '<=':
                            if ((double) $x <= (double) $y) {
                                $offset = true;
                            }
                            break;
                    }
                    $haystack = str_replace($parser, ($offset ? $parser_1 : $parser_2), $haystack);
                }
            }
        }
        return $haystack;
    }

}
?>