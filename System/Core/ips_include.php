<?php

if (!function_exists('ips_include')) {

    function ips_include($include, $object = false) {
        ob_start();
        if (file_exists($include)) {
            include $include;
            if ($object) {
                if (class_exists($object)) {
                    new $object;
                }
            }
        }
        return ob_get_clean();
    }

}
?>