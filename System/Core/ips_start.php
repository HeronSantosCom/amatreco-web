<?php

if (!function_exists('ips_start')) {

    function ips_start($last_start = false) {
        error_reporting(1);
        ini_set('error_reporting', E_ALL);
        ini_set("display_errors", 1);
        set_error_handler('ips_reporting');
        register_shutdown_function('ips_reporting');
        date_default_timezone_set(system_timezone);
        session_start();
        //unset ($_SESSION);
        if (isset($_SESSION["IPS"]["PARSER"])) {
            unset($_SESSION["IPS"]["PARSER"]);
        }
        ob_start();
    }

}
?>