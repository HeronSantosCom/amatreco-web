<?php

if (!function_exists('ips_path')) {

    function ips_path($path, $cache = false) {
        $path = ips_locate("remote") . "/" . $path;
        if ($cache) {
            $path .= ( strpos($path, "?") ? "&" : "?") . time();
        }
        return $path;
    }

}
?>