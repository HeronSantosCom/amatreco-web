<?php

if (!function_exists('ips_for')) {

    function ips_for($condition, $haystack) {
        $function = $condition[0];
        $name = trim($condition[3]);
        $condition = trim($condition[2]);
        $init = strpos($haystack, "{%{$function}%}");
        $parser = substr($haystack, $init);
        $end = strpos($parser, "{% endfor:{$name} %}") + strlen("{% endif:{$name} %}");
        $parser = substr($parser, 0, $end+1);
        $offset = substr($parser, strlen("{%{$function}%}"), -strlen("{% endif:{$name} %}")-1);
        if (preg_match('/(.*) (.*)/i', $condition, $function)) {
            if (isset($function[2])) {
                $start = trim($function[1]);
                $end = trim($function[2]);
                for ($x = $start; $x <= $end; $x++) {
                    $replace[] = str_replace("{% this:{$name} %}", $x, $offset);
                }
                $haystack = str_replace($parser, (isset($replace) ? join("\n", $replace) : null), $haystack);
            }
        }
        return $haystack;
    }

}
?>