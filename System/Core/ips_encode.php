<?php

if (!function_exists('ips_encode')) {

    function ips_encode($value) {
        $value = base64_encode($value);
        $value = str_replace(array('+', '/', '='), array('-', '_', '.'), $value);
        return $value;
    }

}
?>