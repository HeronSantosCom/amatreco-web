<?php

if (!function_exists('get_called_class')) {

    function get_called_class() {
        foreach (debug_backtrace () as $trace) {
            if (isset($trace['object'])) {
                if ($trace['object'] instanceof $trace['class']) {
                    return get_class($trace['object']);
                }
            }
        }
        return false;
    }

}
?>