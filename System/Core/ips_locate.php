<?php

if (!function_exists('ips_locate')) {

    function ips_locate($type = false, $sufix = false) {
        if (!isset($_SESSION["IPS"]["PARSER"]["LOCATION"])) {
            $_SESSION["IPS"]["PARSER"]["LOCATION"] = false;
            $dirpath = pathinfo(__file__); //retorna as informacoes do diretorio
            $dirpath = str_replace("\\", "/", $dirpath["dirname"]); //corrige nome de diretorio windows
            $dirlocal = explode('/System', $dirpath);  //extrai o caminho do diretorio local
            $_SESSION["IPS"]["PARSER"]["LOCATION"]["LOCAL"] = $dirlocal[0];
            $dirlocal = explode('/', $dirlocal[0]);
            $dirname = array_pop($dirlocal); //extrai o diretorio local
            $_SESSION["IPS"]["PARSER"]["LOCATION"]["DIRECTORY"] = "/{$dirname}";
            $dirpath = explode("/{$dirname}", $_SESSION["IPS"]["PARSER"]["LOCATION"]["LOCAL"]); //extrai o caminho para o diretorio local
            $_SESSION["IPS"]["PARSER"]["LOCATION"]["PATH"] = $dirpath[0];
            if (isset($_SERVER['REQUEST_URI'])) {
                $diruri = null;
                if (preg_match("#{$_SESSION["IPS"]["PARSER"]["LOCATION"]["DIRECTORY"]}#", $_SERVER['REQUEST_URI'])) {
                    $diruri = explode($_SESSION["IPS"]["PARSER"]["LOCATION"]["DIRECTORY"], $_SERVER['REQUEST_URI'], 2); //extrai o caminho remoto
                    $diruri = $diruri[0] . $_SESSION["IPS"]["PARSER"]["LOCATION"]["DIRECTORY"];
                }
                $_SESSION["IPS"]["PARSER"]["LOCATION"]["REMOTE"] = $diruri;
                $httppath = "http://127.0.0.1";
                if (isset($_SERVER["HTTP_HOST"]) and strlen($_SERVER["HTTP_HOST"]) > 0) {
                    $httpprefix = "http://";
                    if (isset($_SERVER["HTTPS"])) {
                        $httpprefix = "https://";
                    }
                    $httppath = $httpprefix . $_SERVER["HTTP_HOST"];
                }
                $_SESSION["IPS"]["PARSER"]["LOCATION"]["URI"] = $httppath . $diruri;
            }
        }
        if ($type) {
            if (isset($_SESSION["IPS"]["PARSER"]["LOCATION"][strtoupper($type)])) {
                return $_SESSION["IPS"]["PARSER"]["LOCATION"][strtoupper($type)] . ($sufix ? $sufix : null);
            }
            return ($sufix ? $sufix : false);
        }
        return $_SESSION["IPS"]["PARSER"]["LOCATION"];
    }

}
?>