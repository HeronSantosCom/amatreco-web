<?php

if (!function_exists('ips_halt')) {

    function ips_halt() {
        ips_exit(ob_get_clean());
    }

}
?>