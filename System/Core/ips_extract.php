<?php

if (!function_exists('ips_extract')) {

    function ips_extract($array, $prefix = null, $sufix = null) {
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                ips_set("{$prefix}{$key}{$sufix}", $value);
            }
        }
    }

}
?>