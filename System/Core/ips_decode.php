<?php

if (!function_exists('ips_decode')) {

    function ips_decode($value) {
        $value = str_replace(array('-', '_', '.'), array('+', '/', '='), $value);
        $mod4 = strlen($value) % 4;
        if ($mod4) {
            $value .= substr('====', $mod4);
        }
        return base64_decode($value);
    }

}
?>