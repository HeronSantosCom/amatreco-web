<?php

if (!function_exists('ips_get')) {

    function ips_get($name) {
        if (isset($_SESSION["IPS"]["PARSER"]["VAR"][$name])) {
            return $_SESSION["IPS"]["PARSER"]["VAR"][$name];
        }
        return false;
    }

}
?>