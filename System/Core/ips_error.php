<?php

if (!function_exists('ips_error')) {

    function ips_error($message = false, $code = 500) {
        if (system_show_erro) {
            ob_clean();
            switch ($code) {
                case 404:
                    $codemsg = "404 Not Found";
                    break;
                case 500:
                    $codemsg = "500 Internal Server Error";
                    break;
            }
            header("HTTP/1.0 {$codemsg}");
            header("Status: {$codemsg}");
            $_SERVER['REDIRECT_STATUS'] = (int) $code;
            $debug = debug_backtrace();
            $trace = (is_array($debug) ? array_splice($debug, 1) : false);
            $print[] = "<h1>Ocorreu um erro ao carregar a página!</h1>\n";
            if ($message) {
                $print[] = "<hr />";
                $print[] = "<h2>Descrição:</h2>\n";
                $print[] = "<p>{$message}<p>\n";
                if (is_array($trace)) {
                    $print[] = "<pre>\n";
                    $print[] = "<b>Execução em Tempo Real:</b>\n";
                    foreach ($trace as $key => $line) {
                        if (isset($line["file"])) {
                            $print[] = "<i>{$line["file"]}</i>" . (isset($line["line"]) ? "<b>: linha {$line["line"]}</b>" : null) . "\n";
                        }
                    }
                    $print[] = "</pre>\n";
                }
            }
            $print[] = "<hr />";
            $print[] = "<p>";
            $print[] = "<b>Insign Platform System 1.2</b><br />\n";
            $print[] = "Notificação gerada em <i>" . date("r") . "</i> registrada sobre identificação <i>" . session_id() . "</i> (<i>" . $_SERVER["REMOTE_ADDR"] . "</i> / <i>" . gethostbyaddr($_SERVER["REMOTE_ADDR"]) . "</i>)\n";
            $print[] = "</p>";
            ips_exit(join("", $print), "ALL");
        }
    }

}
?>