<?php

if (!function_exists('ips_pagination')) {

    function ips_pagination($page, $rows, $sufix = false) {
        $sufix = ($sufix ? " {$sufix}" : null);
        ips_set("This Page{$sufix}", $page);
        ips_set("Total Rows{$sufix}", $rows);
        if (ips_get("Total Rows{$sufix}") > 0) {
            ips_set("Total Pages{$sufix}", ceil(ips_get("Total Rows{$sufix}") / ips_define("System Max Grid")));
            if (ips_get("This Page{$sufix}") > ips_get("Total Pages{$sufix}")) {
                ips_set("This Page{$sufix}", ips_get("Total Pages{$sufix}"));
            }
            if (ips_get("This Page{$sufix}") > 1) {
                ips_set("First Page{$sufix}", 1);
                ips_set("Previous Page{$sufix}", (ips_get("This Page{$sufix}") - 1));
            }
            if (ips_get("This Page{$sufix}") < ips_get("Total Pages{$sufix}") and ips_get("Total Pages{$sufix}") > 1) {
                ips_set("Next Page{$sufix}", (ips_get("This Page{$sufix}") + 1));
                ips_set("Last Page{$sufix}", ips_get("Total Pages{$sufix}"));
            }
            ips_set("First Row{$sufix}", ((ips_define("System Max Grid") * ips_get("This Page{$sufix}")) - ips_define("System Max Grid")));
            ips_set("Last Row{$sufix}", (ips_get("Total Rows{$sufix}") > ips_define("System Max Grid") * ips_get("This Page{$sufix}") ? (ips_define("System Max Grid") * ips_get("This Page{$sufix}"))-1 : ips_get("Total Rows{$sufix}")-1));
        }
    }

}
?>