<?php

if (!function_exists('ips_permalink')) {

    function ips_permalink($get = false, $starter = false) {
        if (!isset($_SESSION["IPS"]["PARSER"]["PERMALINK"]) or $starter) {
            if (isset($_SERVER['REQUEST_URI'])) {
                $_SESSION["IPS"]["PARSER"]["PERMALINK"] = false;
                $directory = ips_locate("directory");
                if (preg_match("#$directory#", $_SERVER['REQUEST_URI'])) {
                    $dir = explode($directory, $_SERVER['REQUEST_URI'], 2);
                    $starter = "{$dir[0]}{$directory}{$starter}";
                }
                $starter .= "/";
                $permalink = explode($starter, $_SERVER['REQUEST_URI'], 2);
                if (isset($permalink[1])) {
                    $permalink = explode("?", $permalink[1], 2);
                    $permalink = explode("#", $permalink[0], 2);
                    $permalink = $permalink[0];
                    if (substr($permalink, strlen($permalink) - 1) == "/") {
                        $permalink = substr($permalink, 0, strlen($permalink) - 1);
                    }
                    $permalink = explode("/", $permalink);
                    if (count($permalink) == 1 and strlen($permalink[0]) == 0) {
                        $permalink = false;
                    }
                    if (is_array($permalink)) {
                        foreach ($permalink as $column => $value) {
                            $_SESSION["IPS"]["PARSER"]["PERMALINK"][$column + 1] = $value;
                        }
                    }
                }
            }
        }
        if (isset($_SESSION["IPS"]["PARSER"]["PERMALINK"])) {
            if ($get > 0) {
                if (isset($_SESSION["IPS"]["PARSER"]["PERMALINK"][$get])) {
                    return $_SESSION["IPS"]["PARSER"]["PERMALINK"][$get];
                }
                return false;
            }
            return $_SESSION["IPS"]["PARSER"]["PERMALINK"];
        }
        return false;
    }

}
?>