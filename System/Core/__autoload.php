<?php

if (!function_exists('__autoload')) {

    function __autoload($class_name) {
        ips_include(system_application_path . "/{$class_name}.php");
    }

}
?>