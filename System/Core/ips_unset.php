<?php

if (!function_exists('ips_unset')) {

    function ips_unset($name, $type = false) {
        if ($type == "global") {
            if (isset($_SESSION["IPS"]["GLOBAL"][$name])) {
                unset($_SESSION["IPS"]["GLOBAL"][$name]);
                return true;
            }
            return false;
        }
        if (isset($_SESSION["IPS"]["PARSER"]["VAR"][$name])) {
            unset($_SESSION["IPS"]["PARSER"]["VAR"][$name]);
            return true;
        }
        return false;
    }

}
?>