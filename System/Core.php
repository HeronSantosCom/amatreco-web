<?php

class Core {

    public function __construct() {
        $this->Core();
        $this->Library();
        $this->Application();
    }

    private function Core() {
        $array = array_diff(scandir("System/Core/"), array('.', '..'));
        foreach ($array as $filename) {
            if (strpos($filename, ".php")) {
                include "System/Core/{$filename}";
            }
        }
    }

    private function Library() {
        $array = array_diff(scandir("Library/"), array('.', '..'));
        foreach ($array as $filename) {
            if (strpos($filename, ".php")) {
                include "Library/{$filename}";
            }
        }
    }

    private function Application() {
        $permalink = ips_permalink();
        if ($permalink) {
            $starter = false;
            foreach ($permalink as $item) {
                if (strlen($item) > 0) {
                    $item = $starter . "/{$item}";
                    if (!file_exists("Application{$item}/")) {
                        break;
                    }
                    $starter = $item;
                }
            }
        }
        define("system_application_path", "Application" . (isset($starter) ? $starter : "/"));
        ips_start();
        ips_include("Application/Globals.php");
        print ips_source(ips_include(system_application_path . "/Core.php") . ips_include(system_application_path . "/Structure.html"));
        ips_halt();
    }

}

?>