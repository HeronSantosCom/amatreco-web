<?php

define("system_show_erro", true);
define("system_title", "Associação de Moradores do Bairro Três Corações");
//define("system_title", "Administração");
define("system_max_grid", "20");
define("system_request_uri", $_SERVER['REQUEST_URI']);
define("system_timezone", "America/Sao_Paulo");
define("mysql_server", "localhost");
define("mysql_database", "amatreco");
define("mysql_username", "root");
define("mysql_password", "root");
define("mysql_limit", "3000");
define("mysql_timezone", "-3:00");
define("mysql_encoding", "utf8");

header('Content-type: text/html; charset=utf-8');
include("System/Core.php");
new Core();
?>
