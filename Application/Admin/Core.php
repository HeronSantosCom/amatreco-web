<?php

if (!ips_global("Profile")) {
    ips_redirect(ips_locate("remote", "/Admin/Login?" . base64_encode(system_request_uri)));
}
ips_set("Starter", "Dashboard");
$permalink = ips_permalink(false, "/Admin");
if ($permalink) {
    ips_set("Starter", join("/", $permalink));
}
$profile = ips_global("Profile");
ips_set("Profile Name", $profile["Name"]);
ips_set("Profile Account", $profile["Account"]);
?>
