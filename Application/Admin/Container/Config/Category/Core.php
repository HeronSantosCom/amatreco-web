<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doCount() {
    $MySQL = ips_define("Connection MySQL");
    $IPSCategory = $MySQL->table("IPSCategory")->column("COUNT(*)", "total")->select();
    if (isset($IPSCategory[0]["total"])) {
        return $IPSCategory[0]["total"];
    }
    return 0;
}

function doRows() {
    $MySQL = ips_define("Connection MySQL");
    $MySQL->table("IPSCategory");
    $IPSCategory = $MySQL->column("*")->limit(ips_define("System Max Grid"), ips_get("First Row"))->select();
    if ($IPSCategory) {
        foreach ($IPSCategory as $key => $fields) {
            if (is_array($fields)) {
                $key += ips_get("First Row");
                foreach ($fields as $field => $value) {
                    ips_set("IPSCategory $field {$key}", $value);
                }
            }
        }
    }
}

ips_extract($_GET);
$total = doCount();
ips_pagination((ips_permalink(5) > 0 ? ips_permalink(5) : 1), $total);
doRows();

?>
