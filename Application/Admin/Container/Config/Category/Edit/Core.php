<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doGet($idIPSCategory = false) {
    if ($idIPSCategory > 0) {
        $MySQL = ips_define("Connection MySQL");
        $IPSCategory = $MySQL->table("IPSCategory")->where("idIPSCategory", "=", $idIPSCategory)->select();
        if ($IPSCategory) {
            foreach ($IPSCategory[0] as $field => $value) {
                ips_set($field, stripslashes($value));
            }
        }
    }
    ips_extract($_POST);
}

function doSet($array = false) {
    $MySQL = ips_define("Connection MySQL");
    $IPSCategory = $MySQL->table("IPSCategory")->fields();
    if ($IPSCategory) {
        foreach ($IPSCategory as $row) {
            $array[$row] = ips_get($row);
        }
    }
    return (is_array($array) ? $array : false);
}

function doSave($idIPSCategory = false) {
    $MySQL = ips_define("Connection MySQL");
    $columns = doSet();
    if ($columns) {
        $MySQL->table("IPSCategory");
        foreach ($columns as $key => $value) {
            $MySQL->column($key, $value);
        }
        if ($idIPSCategory) {
            $MySQL->where("idIPSCategory", "=", $idIPSCategory);
        }
        $IPSCategory = $MySQL->save();
        if (!$IPSCategory[0]) {
            return false;
        }
        return true;
    }
    return false;
}

doGet(ips_permalink(6));
if (ips_get("doConfirm")) {
    ips_set("Update", "NOW()");
    ips_set("Submit", "ERRO");
    ips_set("Status", "1");
    if (doSave (ips_permalink(6))) {
        ips_redirect(ips_locate("remote", "/Admin/Container/Config/Category/?Submit=SUCCESS"));
    }
}
?>
