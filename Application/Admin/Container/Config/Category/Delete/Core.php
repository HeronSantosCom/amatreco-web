<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doSave($idIPSCategory = false) {
    $MySQL = ips_define("Connection MySQL");
    if ($idIPSCategory > 0) {
        $MySQL->table("IPSCategory");
        $MySQL->where("idIPSCategory", " IN ", "({$idIPSCategory})");
        $IPSCategory = $MySQL->delete();
        if ($IPSCategory[0]) {
            return true;
        }
    }
    return false;
}

if (doSave(ips_permalink(6))) {
    ips_redirect(ips_locate("remote", "/Admin/Container/Config/Category/?Submit=SUCCESS"));
}
ips_redirect(ips_locate("remote", "/Admin/Container/Config/Category/?Submit=ERRO"));
?>
