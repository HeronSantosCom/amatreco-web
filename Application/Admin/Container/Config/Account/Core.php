<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doCount() {
    $MySQL = ips_define("Connection MySQL");
    $IPSAccount = $MySQL->table("IPSAccount")->column("COUNT(*)", "total")->where("Account", "<>", "root")->select();
    if (isset($IPSAccount[0]["total"])) {
        return $IPSAccount[0]["total"];
    }
    return 0;
}

function doRows() {
    $MySQL = ips_define("Connection MySQL");
    $MySQL->table("IPSAccount");
    $IPSAccount = $MySQL->column("*")->column("DATE_FORMAT(`Insert`, '%d/%m/%Y %H:%i:%s')", "Insert")->where("Account", "<>", "root")->limit(ips_define("System Max Grid"), ips_get("First Row"))->select();
    if ($IPSAccount) {
        foreach ($IPSAccount as $key => $fields) {
            if (is_array($fields)) {
                $key += ips_get("First Row");
                foreach ($fields as $field => $value) {
                    ips_set("IPSAccount $field {$key}", $value);
                }
            }
        }
    }
}

ips_extract($_GET);
$total = doCount();
ips_pagination((ips_permalink(5) > 0 ? ips_permalink(5) : 1), $total);
doRows();

?>
