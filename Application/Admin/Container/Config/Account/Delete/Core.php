<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doSave($idIPSAccount = false) {
    $MySQL = ips_define("Connection MySQL");
    if ($idIPSAccount > 0) {
        $MySQL->table("IPSAccount");
        $MySQL->where("idIPSAccount", " IN ", "({$idIPSAccount})");
        $IPSAccount = $MySQL->delete();
        if ($IPSAccount[0]) {
            return true;
        }
    }
    return false;
}

if (doSave(ips_permalink(6))) {
    ips_redirect(ips_locate("remote", "/Admin/Container/Config/Account/?Submit=SUCCESS"));
}
ips_redirect(ips_locate("remote", "/Admin/Container/Config/Account/?Submit=ERRO"));
?>
