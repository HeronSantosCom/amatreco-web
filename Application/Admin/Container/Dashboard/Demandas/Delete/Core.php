<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doSave($idIPSPost = false) {
    $MySQL = ips_define("Connection MySQL");
    if ($idIPSPost > 0) {
        $MySQL->table("IPSPost");
        $MySQL->where("idIPSPost", " IN ", "({$idIPSPost})");
        $IPSPost = $MySQL->delete();
        if ($IPSPost[0]) {
            return true;
        }
    }
    return false;
}

if (doSave(ips_permalink(6))) {
    ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/Demandas/?Submit=SUCCESS"));
}
ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/Demandas/?Submit=ERRO"));
?>
