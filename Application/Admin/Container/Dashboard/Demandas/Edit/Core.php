<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

//function doIPSCategory() {
//    $MySQL = ips_define("Connection MySQL");
//    $IPSCategory = $MySQL->table("IPSCategory")->select();
//    if ($IPSCategory) {
//        ips_set("First Row IPSCategory", 1);
//        ips_set("Last Row IPSCategory", count($IPSCategory));
//        foreach ($IPSCategory as $key => $fields) {
//            $key++;
//            if (is_array($fields)) {
//                foreach ($fields as $field => $value) {
//                    ips_set("IPSCategory $field {$key}", $value);
//                }
//            }
//        }
//    }
//}
//
//function doIPSSource() {
//    $MySQL = ips_define("Connection MySQL");
//    $IPSSource = $MySQL->table("IPSSource")->select();
//    if ($IPSSource) {
//        ips_set("First Row IPSSource", 1);
//        ips_set("Last Row IPSSource", count($IPSSource));
//        foreach ($IPSSource as $key => $fields) {
//            $key++;
//            if (is_array($fields)) {
//                foreach ($fields as $field => $value) {
//                    ips_set("IPSSource $field {$key}", $value);
//                }
//            }
//        }
//    }
//}

function doGet($idIPSPost = false) {
    if ($idIPSPost > 0) {
        $MySQL = ips_define("Connection MySQL");
        $IPSPost = $MySQL->table("IPSPost")->where("idIPSPost", "=", $idIPSPost)->select();
        if ($IPSPost) {
            foreach ($IPSPost[0] as $field => $value) {
                ips_set($field, stripslashes($value));
            }
        }
    }
    ips_extract($_POST);
}

function doSet($array = false) {
    $MySQL = ips_define("Connection MySQL");
    $IPSPost = $MySQL->table("IPSPost")->fields();
    if ($IPSPost) {
        foreach ($IPSPost as $row) {
            $array[$row] = ips_get($row);
        }
    }
    return (is_array($array) ? $array : false);
}

function doSave($idIPSPost = false) {
    $MySQL = ips_define("Connection MySQL");
    $columns = doSet();
    if ($columns) {
        $MySQL->table("IPSPost");
        foreach ($columns as $key => $value) {
            $MySQL->column($key, $value);
        }
        if ($idIPSPost) {
            $MySQL->where("idIPSPost", "=", $idIPSPost);
        }
        $IPSPost = $MySQL->save();
        if (!$IPSPost[0]) {
            return false;
        }
        return true;
    }
    return false;
}

doGet(ips_permalink(6));
if (ips_get("doConfirm")) {
    $profile = ips_global("Profile");
    ips_set("idIPSAccount", $profile["idIPSAccount"]);
    ips_set("Update", "NOW()");
    ips_set("Submit", "ERRO");
    ips_set("Status", "1");
    if (doSave(ips_permalink(6))) {
        ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/Demandas/?Submit=SUCCESS"));
    }
}
//doIPSCategory();
//doIPSSource();
?>
