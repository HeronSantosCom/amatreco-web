<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doGet($idProjetos = false) {
    if ($idProjetos > 0) {
        $MySQL = ips_define("Connection MySQL");
        $Projetos = $MySQL->table("Projetos")->where("idProjetos", "=", $idProjetos)->select();
        if ($Projetos) {
            foreach ($Projetos[0] as $field => $value) {
                ips_set($field, stripslashes($value));
            }
        }
    }
    ips_extract($_POST);
}

function doSet($array = false) {
    $MySQL = ips_define("Connection MySQL");
    $Projetos = $MySQL->table("Projetos")->fields();
    if ($Projetos) {
        foreach ($Projetos as $row) {
            $array[$row] = ips_get($row);
        }
    }
    return (is_array($array) ? $array : false);
}

function doSave($idProjetos = false) {
    $MySQL = ips_define("Connection MySQL");
    $columns = doSet();
    if ($columns) {
        $MySQL->table("Projetos");
        foreach ($columns as $key => $value) {
            $MySQL->column($key, $value);
        }
        if ($idProjetos) {
            $MySQL->where("idProjetos", "=", $idProjetos);
        }
        $Projetos = $MySQL->save();
        if (!$Projetos[0]) {
            return false;
        }
        return true;
    }
    return false;
}

doGet(ips_permalink(6));
if (ips_get("doConfirm")) {
    $profile = ips_global("Profile");
    ips_set("idIPSAccount", $profile["idIPSAccount"]);
    ips_set("Update", "NOW()");
    ips_set("Submit", "ERRO");
    ips_set("Status", "1");
    if (doSave(ips_permalink(6))) {
        ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/Projetos/?Submit=SUCCESS"));
    }
}
?>
