<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doSave($idProjetos = false) {
    $MySQL = ips_define("Connection MySQL");
    if ($idProjetos > 0) {
        $MySQL->table("Projetos");
        $MySQL->where("idProjetos", " IN ", "({$idProjetos})");
        $Projetos = $MySQL->delete();
        if ($Projetos[0]) {
            return true;
        }
    }
    return false;
}

if (doSave(ips_permalink(6))) {
    ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/Projetos/?Submit=SUCCESS"));
}
ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/Projetos/?Submit=ERRO"));
?>
