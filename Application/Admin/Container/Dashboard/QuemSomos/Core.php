<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doCount() {
    $MySQL = ips_define("Connection MySQL");
    $QuemSomos = $MySQL->table("QuemSomos")->column("COUNT(*)", "total")->select();
    if (isset($QuemSomos[0]["total"])) {
        return $QuemSomos[0]["total"];
    }
    return 0;
}

function doRows() {
    $MySQL = ips_define("Connection MySQL");
    $MySQL->table("QuemSomos");
    $QuemSomos = $MySQL->column("*")->limit(ips_define("System Max Grid"), ips_get("First Row"))->order($MySQL->get->column("Prioridade", null, 1), "DESC")->order($MySQL->get->column("Nome", null, 1), "ASC")->select();
    if ($QuemSomos) {
        foreach ($QuemSomos as $key => $fields) {
            if (is_array($fields)) {
                $key += ips_get("First Row");
                foreach ($fields as $field => $value) {
                    ips_set("QuemSomos $field {$key}", $value);
                }
            }
        }
    }
}

ips_extract($_GET);
$total = doCount();
ips_pagination((ips_permalink(5) > 0 ? ips_permalink(5) : 1), $total);
doRows();

?>
