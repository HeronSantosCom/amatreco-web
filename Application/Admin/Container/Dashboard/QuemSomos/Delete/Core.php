<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode(str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null)))) . '", "_parent")</script>');
}

function doSave($idQuemSomos = false) {
    $MySQL = ips_define("Connection MySQL");
    if ($idQuemSomos > 0) {
        $Avatar = false;
        $QuemSomos = $MySQL->table("QuemSomos")->column("Avatar")->where("idQuemSomos", "=", $idQuemSomos)->select();
        if ($QuemSomos) {
            $Avatar = $QuemSomos[0]['Avatar'];
        }
        $MySQL->table("QuemSomos");
        $MySQL->where("idQuemSomos", " IN ", "({$idQuemSomos})");
        $QuemSomos = $MySQL->delete();
        if ($QuemSomos[0]) {
            unlink(ips_locate("local", "/Gallery/Amatreco/QuemSomos/" . ips_decode($Avatar)));
            return true;
        }
    }
    return false;
}

if (doSave(ips_permalink(6))) {
    ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/QuemSomos/?Submit=SUCCESS"));
}
ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/QuemSomos/?Submit=ERRO"));
?>
