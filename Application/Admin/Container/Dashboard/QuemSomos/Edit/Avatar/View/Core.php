<?php

header("Content-Type: image/png");
ips_extract($_GET);
$file = ips_locate("local", "/Gallery/ControlAdmin/images/noimage.png");
if (strlen(ips_get("Avatar")) > 0) {
    $Avatar = (ips_get("Cache") != "1" ? ips_locate("local", "/Gallery/Amatreco/QuemSomos/" . ips_decode(ips_get("Avatar"))) : ips_locate("local", "/Cache/" . ips_decode(ips_get("Avatar"))));
    if (file_exists($Avatar)) {
        $file = $Avatar;
    }
}
$handle = fopen($file, "r");
$contents = fread($handle, filesize($file));
fclose($handle);
print $contents;
exit;
?>