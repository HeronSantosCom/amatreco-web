<?php

$Avatar = uniqid() . ".png";
$file = ips_locate("local", "/Cache/{$Avatar}");
if (!empty($_FILES)) {
    $tmp = $_FILES['Filedata']['tmp_name'];
    $thumb = new Thumbnail($tmp);
    $thumb->size_auto(128);
    $thumb->output_format = 'PNG';
    $thumb->process();
    $thumb->save($file);
    if (file_exists($file)) {
        print ips_encode($Avatar);
    }
}
?>