<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode(str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null)))) . '", "_parent")</script>');
}

function doGet($idQuemSomos = false) {
    if ($idQuemSomos > 0) {
        $MySQL = ips_define("Connection MySQL");
        $QuemSomos = $MySQL->table("QuemSomos")->where("idQuemSomos", "=", $idQuemSomos)->select();
        if ($QuemSomos) {
            foreach ($QuemSomos[0] as $field => $value) {
                ips_set($field, stripslashes($value));
            }
        }
    }
    ips_extract($_POST);
}

function doSet($array = false) {
    $MySQL = ips_define("Connection MySQL");
    $QuemSomos = $MySQL->table("QuemSomos")->fields();
    if ($QuemSomos) {
        foreach ($QuemSomos as $row) {
            $array[$row] = ips_get($row);
        }
    }
    return (is_array($array) ? $array : false);
}

function doSave($idQuemSomos = false) {
    $MySQL = ips_define("Connection MySQL");
    $columns = doSet();
    if ($columns) {
        $MySQL->table("QuemSomos");
        foreach ($columns as $key => $value) {
            $MySQL->column($key, $value);
        }
        if ($idQuemSomos) {
            $MySQL->where("idQuemSomos", "=", $idQuemSomos);
        }
        $QuemSomos = $MySQL->save();
        if (!$QuemSomos[0]) {
            return false;
        }
        return true;
    }
    return false;
}

doGet(ips_permalink(6));
if (ips_get("doConfirm")) {
    if (ips_get("Avatar") != ips_get("AvatarOld")) {
        if (strlen(ips_get("Avatar")) > 0) {
            $cache = ips_locate("local", "/Cache/" . ips_decode(ips_get("Avatar")));
            $save = ips_locate("local", "/Gallery/Amatreco/QuemSomos/" . ips_decode(ips_get("Avatar")));
            copy($cache, $save);
        }
        if (strlen(ips_get("AvatarOld")) > 0) {
            unlink(ips_get("AvatarOld"));
        }
    }
    $profile = ips_global("Profile");
    ips_set("idIPSAccount", $profile["idIPSAccount"]);
    ips_set("Update", "NOW()");
    ips_set("Submit", "ERRO");
    ips_set("Status", "1");
    if (doSave(ips_permalink(6))) {
        ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/QuemSomos/?Submit=SUCCESS"));
    }
}
?>
