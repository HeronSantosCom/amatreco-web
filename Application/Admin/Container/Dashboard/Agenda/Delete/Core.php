<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doSave($idAgenda = false) {
    $MySQL = ips_define("Connection MySQL");
    if ($idAgenda > 0) {
        $MySQL->table("Agenda");
        $MySQL->where("idAgenda", " IN ", "({$idAgenda})");
        $Agenda = $MySQL->delete();
        if ($Agenda[0]) {
            return true;
        }
    }
    return false;
}

if (doSave(ips_permalink(6))) {
    ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/Agenda/?Submit=SUCCESS"));
}
ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/Agenda/?Submit=ERRO"));
?>
