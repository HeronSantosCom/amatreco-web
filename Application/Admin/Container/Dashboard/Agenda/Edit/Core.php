<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode(str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null)))) . '", "_parent")</script>');
}

function doGet($idAgenda = false) {
    if ($idAgenda > 0) {
        $MySQL = ips_define("Connection MySQL");
        $Agenda = $MySQL->table("Agenda")->where("idAgenda", "=", $idAgenda)->select();
        if ($Agenda) {
            foreach ($Agenda[0] as $field => $value) {
                ips_set($field, stripslashes($value));
            }
        }
    }
    ips_extract($_POST);
}

function doSet($array = false) {
    $MySQL = ips_define("Connection MySQL");
    $Agenda = $MySQL->table("Agenda")->fields();
    if ($Agenda) {
        foreach ($Agenda as $row) {
            $array[$row] = ips_get($row);
        }
    }
    return (is_array($array) ? $array : false);
}

function doSave($idAgenda = false) {
    $MySQL = ips_define("Connection MySQL");
    $columns = doSet();
    if ($columns) {
        $MySQL->table("Agenda");
        foreach ($columns as $key => $value) {
            $MySQL->column($key, $value);
        }
        if ($idAgenda) {
            $MySQL->where("idAgenda", "=", $idAgenda);
        }
        $Agenda = $MySQL->save();
        if (!$Agenda[0]) {
            return false;
        }
        return true;
    }
    return false;
}

function getDateFormat($date, $in = "dd/mm/yyyy", $out = "yyyy-mm-dd") {
    $in = strtoupper($in);
    $out = strtoupper($out);
    $day = substr($date, strpos($in, "DD"), 2);
    $return = str_replace("DD", $day, $out);
    $month = substr($date, strpos($in, "MM"), 2);
    $return = str_replace("MM", $month, $return);
    $year = substr($date, strpos($in, "YYYY"), 4);
    $return = str_replace("YYYY", $year, $return);
    if (strlen($in) == 8) {
        $year = substr($date, strpos($in, "YY"), 2);
        $return = str_replace("YY", $year, $return);
    }
    return $return;
}

ips_set("Data", ips_date(date("d/m/Y")));
ips_set("Hora", "12:00");
doGet(ips_permalink(6));
if (ips_get("doConfirm")) {
    $profile = ips_global("Profile");
    ips_set("idIPSAccount", $profile["idIPSAccount"]);
    ips_set("Data", ips_date(ips_get("Data")));
    ips_set("Update", "NOW()");
    ips_set("Submit", "ERRO");
    ips_set("Status", "1");
    if (doSave(ips_permalink(6))) {
        ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/Agenda/?Submit=SUCCESS"));
    }
}
ips_set("Data", ips_date(ips_get("Data"), "yyyy-mm-dd", "dd/mm/yyyy"));
?>
