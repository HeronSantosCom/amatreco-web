<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doCount() {
    $MySQL = ips_define("Connection MySQL");
    $Agenda = $MySQL->table("Agenda")->column("COUNT(*)", "total")->select();
    if (isset($Agenda[0]["total"])) {
        return $Agenda[0]["total"];
    }
    return 0;
}

function doRows() {
    $MySQL = ips_define("Connection MySQL");
    $MySQL->table("Agenda");
    $Agenda = $MySQL->column("*")->column("CONCAT(DATE_FORMAT(`Data`, '%d/%m/%Y'), ' ', `Hora`)", "Data")->column("DATE_FORMAT(`Insert`, '%d/%m/%Y %H:%i:%s')", "Insert")->limit(ips_define("System Max Grid"), ips_get("First Row"))->order($MySQL->get->column("Data", null, 1), "DESC")->order($MySQL->get->column("Hora", null, 1), "DESC")->select();
    if ($Agenda) {
        foreach ($Agenda as $key => $fields) {
            if (is_array($fields)) {
                $key += ips_get("First Row");
                foreach ($fields as $field => $value) {
                    ips_set("Agenda $field {$key}", $value);
                }
            }
        }
    }
}

ips_extract($_GET);
$total = doCount();
ips_pagination((ips_permalink(5) > 0 ? ips_permalink(5) : 1), $total);
doRows();

?>
