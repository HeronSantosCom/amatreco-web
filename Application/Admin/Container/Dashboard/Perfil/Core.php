<?php

$permalink = ips_permalink(false);
if (!ips_global("Profile")) {
    ips_exit('<script type="text/javascript">doOpen("' . ips_locate("remote", "/Admin/Login?" . base64_encode( str_replace("Admin/Container/", "", ($permalink ? join("/", $permalink) : null) ))) . '", "_parent")</script>');
}

function doGet($idIPSAccount = false) {
    if ($idIPSAccount > 0) {
        $MySQL = ips_define("Connection MySQL");
        $IPSAccount = $MySQL->table("IPSAccount")->where("idIPSAccount", "=", $idIPSAccount)->select();
        if ($IPSAccount) {
            foreach ($IPSAccount[0] as $field => $value) {
                ips_set($field, stripslashes($value));
            }
        }
    }
    ips_extract($_POST);
}

function doSet($array = false) {
    $MySQL = ips_define("Connection MySQL");
    $IPSAccount = $MySQL->table("IPSAccount")->fields();
    if ($IPSAccount) {
        foreach ($IPSAccount as $row) {
            $array[$row] = ips_get($row);
        }
    }
    return (is_array($array) ? $array : false);
}

function doSave($idIPSAccount = false) {
    $MySQL = ips_define("Connection MySQL");
    $columns = doSet();
    if ($columns) {
        $MySQL->table("IPSAccount");
        foreach ($columns as $key => $value) {
            $MySQL->column($key, $value);
        }
        if ($idIPSAccount) {
            $MySQL->where("idIPSAccount", "=", $idIPSAccount);
        }
        $IPSAccount = $MySQL->save();
        if (!$IPSAccount[0]) {
            return false;
        }
        return true;
    }
    return false;
}

$profile = ips_global("Profile");
doGet($profile["idIPSAccount"]);
if (ips_get("doConfirm")) {
    $profile = ips_global("Profile");
    ips_set("idIPSAccount", $profile["idIPSAccount"]);
    ips_set("Account", $profile["Account"]);
    if (strlen(ips_get("Password")) > 0 and ips_get("Password") == ips_get("ConfirmPassword")) {
        ips_set("Password", md5(ips_get("Password")));
    }
    ips_set("Update", "NOW()");
    ips_set("Submit", "ERRO");
    ips_set("Status", "1");
    if (doSave($profile["idIPSAccount"])) {
        ips_redirect(ips_locate("remote", "/Admin/Container/Dashboard/Perfil/?Submit=SUCCESS"));
    }
}
?>
