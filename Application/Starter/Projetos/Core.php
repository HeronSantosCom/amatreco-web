<?php

function doCount() {
    $MySQL = ips_define("Connection MySQL");
    $Projetos = $MySQL->table("Projetos")->column("COUNT(*)", "total")->select();
    if (isset($Projetos[0]["total"])) {
        return $Projetos[0]["total"];
    }
    return 0;
}

function doRows() {
    $MySQL = ips_define("Connection MySQL");
    $MySQL->table("Projetos");
    $Projetos = $MySQL->column("*")->column("DATE_FORMAT(`Insert`, '%d/%m/%Y %H:%i:%s')", "Insert")->limit(ips_define("System Max Grid"), ips_get("First Row"))->order($MySQL->get->column("Insert", null, 1), "DESC")->select();
    if ($Projetos) {
        foreach ($Projetos as $key => $fields) {
            if (is_array($fields)) {
                $key += ips_get("First Row");
                foreach ($fields as $field => $value) {
                    ips_set("Projetos $field {$key}", stripslashes($value));
                }
            }
        }
    }
}

ips_extract($_GET);
$total = doCount();
ips_pagination((ips_permalink(2) > 0 ? ips_permalink(2) : 1), $total);
doRows();

$permalink = ips_permalink(false);
ips_set("IPSPost Retorno", base64_encode(str_replace("Starter/", "", ($permalink ? join("/", $permalink) : null))));
ips_set("System Title", "Projetos");
?>