<?php

ips_extract($_GET);
function doGet($idProjetos = false) {
    if ($idProjetos > 0) {
        $MySQL = ips_define("Connection MySQL");
        $Projetos = $MySQL->table("Projetos")->column("*")->column("DATE_FORMAT(`Insert`, '%d/%m/%Y %H:%i:%s')", "Insert")->where("idProjetos", "=", $idProjetos)->select();
        if ($Projetos) {
            foreach ($Projetos[0] as $field => $value) {
                ips_set($field, stripslashes($value));
            }
        }
    }
}
doGet(ips_permalink(3));
ips_set("Retorno", base64_decode(ips_permalink(4)));
ips_set("System Title", ips_get("Nome"));
ips_set("System Description", ips_get("Descricao"));

?>