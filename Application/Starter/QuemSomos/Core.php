<?php

function doCount() {
    $MySQL = ips_define("Connection MySQL");
    $QuemSomos = $MySQL->table("QuemSomos")->column("COUNT(*)", "total")->select();
    if (isset($QuemSomos[0]["total"])) {
        return $QuemSomos[0]["total"];
    }
    return 0;
}

function doRows() {
    $MySQL = ips_define("Connection MySQL");
    $MySQL->table("QuemSomos");
    $QuemSomos = $MySQL->column("*")->column("DATE_FORMAT(`Insert`, '%d/%m/%Y %H:%i:%s')", "Insert")->limit(ips_define("System Max Grid"), ips_get("First Row"))->order($MySQL->get->column("Prioridade", null, 1), "DESC")->order($MySQL->get->column("Nome", null, 1), "ASC")->select();
    if ($QuemSomos) {
        foreach ($QuemSomos as $key => $fields) {
            if (is_array($fields)) {
                $key += ips_get("First Row");
                foreach ($fields as $field => $value) {
                    $value = stripslashes($value);
                    switch ($field) {
                        case "Email":
                            if (strlen($value) > 0) {
                                $value = " <a href='mailto:{$value}'>{$value} <img src='{% path (Gallery/Amatreco/images/email.png) %}' align='absmiddle' alt='{$value}'/></a>";
                            }
                            break;
                        case "Site":
                            if (strlen($value) > 0) {
                                if (substr(strtolower($value), 0, 4) != "http") {
                                    $value = "http://{$value}";
                                }
                                $value = "<a href='{$value}' target='_blank'><img src='{% path (Gallery/Amatreco/images/site.png) %}' align='absmiddle' alt='{$value}'/></a>";
                            }
                            break;
                        case "Twitter":
                            if (strlen($value) > 0) {
                                if (substr(strtolower($value), 0, 4) != "http") {
                                    $value = "http://{$value}";
                                }
                                $value = "<a href='{$value}' target='_blank'><img src='{% path (Gallery/Amatreco/social/twitter.png) %}' align='absmiddle' alt='{$value}'/></a>";
                            }
                            break;
                        case "Orkut":
                            if (strlen($value) > 0) {
                                if (substr(strtolower($value), 0, 4) != "http") {
                                    $value = "http://{$value}";
                                }
                                $value = "<a href='{$value}' target='_blank'><img src='{% path (Gallery/Amatreco/social/orkut.png) %}' align='absmiddle' alt='{$value}'/></a>";
                            }
                            break;
                        case "Facebook":
                            if (strlen($value) > 0) {
                                if (substr(strtolower($value), 0, 4) != "http") {
                                    $value = "http://{$value}";
                                }
                                $value = "<a href='{$value}' target='_blank'><img src='{% path (Gallery/Amatreco/social/facebook.png) %}' align='absmiddle' alt='{$value}'/></a>";
                            }
                            break;
                    }
                    ips_set("QuemSomos $field {$key}", $value);
                }
            }
        }
    }
}

ips_extract($_GET);
$total = doCount();
ips_pagination((ips_permalink(2) > 0 ? ips_permalink(2) : 1), $total);
doRows();
ips_set("System Title", "Quem Somos");
?>