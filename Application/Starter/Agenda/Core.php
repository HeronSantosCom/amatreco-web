<?php

function doCount() {
    $MySQL = ips_define("Connection MySQL");
    $Agenda = $MySQL->
            where("`Data`", ">=", date("Y-m-d"))->
            table("Agenda")->
            column("COUNT(*)", "total")->
            select();
    if (isset($Agenda[0]["total"])) {
        return $Agenda[0]["total"];
    }
    return 0;
}

function doRows() {
    $MySQL = ips_define("Connection MySQL");
    $MySQL->table("Agenda");
    $Agenda = $MySQL->
            where("`Data`", ">=", date("Y-m-d"))->
            column("*")->
            column("DATE_FORMAT(CONCAT(`Data`, ' ', `Hora`), '%d/%m/%Y')", "Dia")->
            //limit(ips_define("System Max Grid"), ips_get("First Row"))->
            order($MySQL->get->column("Data", null, 1), "DESC")->
            order($MySQL->get->column("Hora", null, 1), "DESC")->
            select();
    if ($Agenda) {
        foreach ($Agenda as $key => $fields) {
            if (is_array($fields)) {
                $key += ips_get("First Row");
                foreach ($fields as $field => $value) {
                    ips_set("Agenda $field {$key}", stripslashes($value));
                }
            }
        }
    }
}


ips_extract($_GET);
$total = doCount();
if ($total > 0) {
    ips_set("First Row", 1);
    ips_set("Last Row", $total);
    ips_set("Total Rows", $total);
}
//ips_pagination((ips_permalink(2) > 0 ? ips_permalink(2) : 1), $total);
doRows();

$permalink = ips_permalink(false);
ips_set("Agenda Retorno", base64_encode(str_replace("Starter/", "", ($permalink ? join("/", $permalink) : null))));
ips_set("System Title", "Agenda");
?>