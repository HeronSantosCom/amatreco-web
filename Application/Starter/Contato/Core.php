<?php

function doEmail($email) {
    $regex = "([a-z0-9_\-\.]+)" . # name
            "@" . # at
            "([a-z0-9\-\.]+){2,255}" . # domain & possibly subdomains
            "\." . # period
            "([a-z]+){2,10}"; # domain extension
    $eregi = preg_replace('/' . $regex . '/', '', $email);
    return empty($eregi) ? true : false;
}

function doSave() {
    if (strlen(ips_get("Nome")) > 0 and strlen(ips_get("Email")) > 0 and strlen(ips_get("Mensagem")) > 0) {
        if (doEmail(ips_get("Email"))) {
            $mail = mail("edson@amatreco.org", "[AMATRECO] Nova mensagem enviada pelo site", ips_get("Mensagem"),
                            "From: " . ips_get("Nome") . " <" . ips_get("Email") . ">\r\n"
                            . "Reply-To: " . ips_get("Nome") . "<" . ips_get("Email") . ">\r\n"
                            . "X-Mailer: PHP/" . phpversion());
            if ($mail) {
                return true;
            }
        }
    }
    return false;
}

ips_extract($_POST);
if (ips_get("Email")) {
    ips_set("Submit", "ERRO");
    if (doSave ()) {
        ips_set("Submit", "SUCCESS");
    }
}
ips_set("System Title", "Contato");
?>