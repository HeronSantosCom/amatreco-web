<?php

function doCount() {
    $MySQL = ips_define("Connection MySQL");
    $IPSPost = $MySQL->table("IPSPost")->where("idIPSCategory", "=", "3")->column("COUNT(*)", "total")->select();
    if (isset($IPSPost[0]["total"])) {
        return $IPSPost[0]["total"];
    }
    return 0;
}

function doRows() {
    $MySQL = ips_define("Connection MySQL");
    $MySQL->table("IPSPost");
    $IPSPost = $MySQL->column("*")->column("DATE_FORMAT(`Insert`, '%d/%m/%Y %H:%i:%s')", "Insert")->where("idIPSCategory", "=", "3")->limit(ips_define("System Max Grid"), ips_get("First Row"))->order($MySQL->get->column("Insert", null, 1), "DESC")->select();
    if ($IPSPost) {
        foreach ($IPSPost as $key => $fields) {
            if (is_array($fields)) {
                $key += ips_get("First Row");
                foreach ($fields as $field => $value) {
                    ips_set("IPSPost $field {$key}", stripslashes($value));
                }
            }
        }
    }
}

ips_extract($_GET);
$total = doCount();
ips_pagination(1, $total);
doRows();

$permalink = ips_permalink(false);
ips_set("IPSPost Retorno", base64_encode(str_replace("Starter/", "", ($permalink ? join("/", $permalink) : null))));
ips_set("System Title", "Página Inicial");
?>