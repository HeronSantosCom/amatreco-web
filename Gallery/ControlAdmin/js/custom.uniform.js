$(document).ready(function() {
	
    // Fire the Uniform Script if checkboxes, radio buttons, or file inputs are found
	
    //    var applyUniform = function() {
    //        $.getScript(IPS_CONTROLADMIN + 'js/jquery.uniform.min.js', function() {
    //            $("input:checkbox, input:radio, input:file").uniform();
    //            $('input:file').after('<div class="clear"></div>')
    //        });
    //    }
    //
    //    if ($('input:checkbox').length > 0){
    //        applyUniform();
    //    }
    //    else if ($('input:radio').length > 0){
    //        applyUniform();
    //    }
    //    else if ($('input:file').length > 0){
    //        applyUniform();
    //    }
    //
    //    // Fire Stylish Select plugin if selects are found
    //
    //    if ($('select').length > 0){
    //        $.getScript(IPS_CONTROLADMIN + 'js/jquery.stylish-select.min.js', function() {
    //            $('select').sSelect({
    //                ddMaxHeight: '200px'
    //            });
    //        });
    //    }
		
    // Fire jHtmlArea and ColorpickerMenu plugins if textarea with a class of full is found.
    //
    //    if ($('textarea.full').length > 0){
    //        $.getScript(IPS_CONTROLADMIN + 'js/jHtmlArea-0.7.0.js', function() {
    //            $.getScript(IPS_CONTROLADMIN + 'js/jHtmlArea.ColorPickerMenu-0.7.0.js', function() {
    //                $("textarea.full").htmlarea({
    //                    toolbar: [
    //                    ["html"], ["bold", "italic", "underline", "strikethrough", "|", "subscript", "superscript"],
    //                    ["forecolor", "increasefontsize", "decreasefontsize"],
    //                    ["orderedlist", "unorderedlist"],
    //                    ["indent", "outdent"],
    //                    ["justifyleft", "justifycenter", "justifyright"],
    //                    ["link", "unlink", "image", "horizontalrule"],
    //                    ["p", "h1", "h2", "h3", "h4", "h5", "h6"],
    //                    ["cut", "copy", "paste"]
    //                    ]
    //                });
    //            });
    //        });
    //    }
		
    // Make the facebox modal plugin run if a link is found (used for profile messages)
		
    //    if ($('a[rel*=facebox]')){
    //        $.getScript(IPS_CONTROLADMIN + 'js/facebox.js', function() {
    //            $('a[rel*=facebox]').facebox();
    //            calcMessageBox();
    //        });
    //    }
    //
    // Close notifications (fade and slideup)
		
    $(".notification a.close").click(function () {
        $(this).parent().fadeTo(400, 0, function () {
            $(this).slideUp(200);
        });
        return false;
    });
		
    // Apply table sorter if a table with class tablesorter is found ( in the examples case, dont sort column 1 or 5)
		
    if ($('table.tablesorter')) {
        $.getScript(IPS_CONTROLADMIN +'js/jquery.tablesorter.min.js', function() {
            $("table.tablesorter").tablesorter({
                headers: {
                    0: {
                        sorter: false
                    },
                    4: {
                        sorter: false
                    }
                }
            });
        });
    }
		
    // Table row hover highlighter and show links
	
    $('table.hover tbody tr').hover(
        function() {  // mouseover
            var item = $(this).attr("id").replace("Item","");
            $(this).children('td').addClass('hover');
            $(this).children('td:nth-child(2)').append('<p class="links"><a href="javascript:doEdit('+item+');">Editar</a><a href="javascript:doView('+item+');">Visualizar</a><a href="javascript:doDelete('+item+');">Remover</a></p>');
        },
        function() {  // mouseout
            $(this).children('td').removeClass('hover');
            $(this).children('td:nth-child(2)').find('p.links').remove();
        });
		   
    // Check all checkboxes in the table when the header checkbox is selected

    $('table thead input[type=checkbox]').click(function(){
        var table = $(this).parents('table');
        if($(this).is(':checked')){
            table.find('div.checker span').addClass('checked');
            table.find('input[type=checkbox]').attr('checked', true);
            table.find('input[type=checkbox]').parents('tr').find('td').addClass('selected');
        }
        else if($(this).is(':not(:checked)')){
            table.find('div.checker span').removeClass('checked');
            table.find('input[type=checkbox]').attr('checked', false);
            table.find('input[type=checkbox]').parents('tr').find('td').removeClass('selected');
        }
    });
		
    $('table tbody input[type=checkbox]').click(function(){
        if($(this).is(':checked')){
            $(this).parents('tr').find('td').addClass('selected');
        }
        else if($(this).is(':not(:checked)')){
            $(this).parents('tr').find('td').removeClass('selected');
        }
    });

    $('table.tablesorter thead input[type=checkbox]').click(function(){
        var table = $(this).parents('table');
        if($(this).is(':checked')){
            table.find('div.checker span').addClass('checked');
            table.find('input[type=checkbox]').attr('checked', true);
        }
        else if($(this).is(':not(:checked)')){
            table.find('div.checker span').removeClass('checked');
            table.find('input[type=checkbox]').attr('checked', false);
        }
    });
    // Textarea limiter

    if ($('textarea.l150').length > 0){
        $.getScript(IPS_CONTROLADMIN +'js/jquery.limit-1.2.js', function() {
            $('textarea.l150').limit('150','#charsLeft');
        });
    }

    if ($('textarea.l250').length > 0){
        $.getScript(IPS_CONTROLADMIN +'js/jquery.limit-1.2.js', function() {
            $('textarea.l250').limit('250','#charsLeft');
        });
    }

    if ($('textarea.l512').length > 0){
        $.getScript(IPS_CONTROLADMIN +'js/jquery.limit-1.2.js', function() {
            $('textarea.l512').limit('510','#charsLeft');
        });
    }

    if ($('textarea.l1024').length > 0){
        $.getScript(IPS_CONTROLADMIN +'js/jquery.limit-1.2.js', function() {
            $('textarea.l1024').limit('1024','#charsLeft');
        });
    }
		
    // Fire Color picker if input[type=color] is found

    if ($('input[type=color]').length > 0){
        $.getScript(IPS_CONTROLADMIN +'js/mColorPicker_min.js');
    }

    if ($("#AvatarFile")) {
        $.getScript(IPS_GALLERY +'Plugins/uploadify/jquery.uploadify.v2.1.0.min.js');
        $.getScript(IPS_GALLERY +'Plugins/uploadify/swfobject.js');
    }

    if ($(".wysiwyg")) {
        $.getScript(IPS_GALLERY +'Plugins/jwysiwyg/jquery.wysiwyg.js');
        (function($){
            $('.wysiwyg').wysiwyg({});
        })(jQuery);
    }
	
});
