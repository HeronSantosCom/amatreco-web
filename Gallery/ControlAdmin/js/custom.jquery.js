/***************************
	  CUSTOM JQUERY
***************************/

// FUNCTIONS

// Fix height of the Container

var calcContainerHeight = function() {
    var headerDimensions = $('#header').height();
    $('#container').height($(window).height() - headerDimensions);
}
  
// Fix height of Content section

var calcContentHeight = function() {
    var headerDimensions = $('#header').height();
    var subheaderDimensions = $('#subheader').height();
    $('#content').height($(window).height() - headerDimensions - subheaderDimensions -1);
}
  
// Make sure the whole of the sidebar menu is always displayed

var calcSidebarHeight = function() {
    var buttonDimensions = $('a.menu').outerHeight();
    var navigationDimensions = $('ul.navigation.visible').outerHeight();
    $('div#sidebar').css('min-height', buttonDimensions + navigationDimensions+ 50);
}
	
// Message box height compared to browser window height

var calcMessageBox = function() {
    $('#facebox .content').height($(window).height() -250);
}
	
// Set up sidebar menu basics

var SidebarMenu = function() {
    if( $('ul.navigation li.current').length>0 ){
        var CurrentNavigation = $('html').find('ul.navigation li.current').parent().attr('id');
        var CurrentNavigationName = $('a[href="'+CurrentNavigation+'"]').html();
        $('a.menu').empty().append(CurrentNavigationName);
        $('ul.navigation').hide();
        $('ul.navigation li.current').parent().addClass('visible').show();
    }
    else{
        var FirstNavigation = $('html').find('ul.navigation:first').attr('id');
        var FirstNavigationName = $('a[href="'+FirstNavigation+'"]').html();
        $('ul#'+FirstNavigation).addClass('current');
        $('a.menu').empty().append(FirstNavigationName);
        $('ul.navigation:not(.current)').hide();
    }
    $('ul.navigation li ul').hide(); //Hide all sub nav's
    if( $('ul.navigation li.current:contains(ul)')){
        $('ul.navigation li.current ul').show();
    }
}
	
// MAIN JQUERY
  
$(document).ready(function() {
	
    // Run functions on window load, and resize
	
    $(window).resize(function() {
        calcContainerHeight();
        calcContentHeight();
        calcMessageBox();
    }).load(function() {
        calcContainerHeight();
        calcContentHeight();
        SidebarMenu();
        calcSidebarHeight();
    });
		
    // Make dropdown menu work on click
		
    $('a.menu').click(function () {
        $('ul#menu').slideDown('fast');
        return false;
    });
		
    // Hide dropdown when user clicks outside of it
	
    $('body').click(function() {
        $('ul#menu:visible').hide();
    });
		
    // Display the correct menu when picked from dropdown menu
	
    $('ul#menu li a').click(function (){
        $('ul#menu:visible').hide();
        var NavigationDestination = $(this).attr('href');
        var NavigationDestinationName = $(this).text();
        if( $('ul#'+NavigationDestination).is(':visible') ) {
        }
        else{
            $('a.menu').empty().append(NavigationDestinationName);
            $('ul.navigation').hide();
            $('ul#'+NavigationDestination).slideDown('fast', function() {
                calcSidebarHeight();
            });
        }
        return false;
    });
		
    // If sub navigation exists, display when the link is clicked, if it's already visible do nothing, if there is no sub navigation, the link functions normally
	
    $('ul.navigation li a').not('ul.navigation li ul li a').click(function (){
        if($(this).siblings('ul:not(:visible)').length){
            $('ul.navigation li ul').slideUp('medium');
            $(this).siblings('ul').slideDown('medium', function(){
                calcSidebarHeight();
            });
            return false;
        } else {
            if ($(this).siblings('ul:visible').length) {
                return false;
            } // Maybe do some more stuff in here...
        }
    });
		
});
