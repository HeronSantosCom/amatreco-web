<?php

include 'MySQL/MySQLInteraction.php';

class MySQL extends MySQLInteraction {

    protected $link;
    protected $validation, $server, $username, $password, $database, $maxlimit, $timezone, $encoding;
    protected $query, $return, $table, $column, $where, $join, $group, $order, $limit, $pointer;
    public $lasterror;

    public function __construct($server, $username, $password, $database, $maxlimit = 3000, $timezone = false, $encoding = false) {
        $this->server = $server;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        $this->maxlimit = $maxlimit;
        $this->timezone = $timezone;
        $this->encoding = $encoding;
        $this->validation = md5(date("r") . uniqid());
        MySQLFunction::connect();
    }

    public function __call($name, $arguments) {
        ips_error("O objeto MySQL::$name(" . join(", ", $arguments) . ") não foi encontrado.");
    }

    public function __get($name) {
        switch ($name) {
            case "get":
                $this->return = true;
                return $this;
                break;
        }
        ips_error("O objeto MySQL::$name não foi encontrado.");
    }

    public function __set($name, $value) {
        $this->$name = $value;
        return $this;
    }

    public function __sleep() {
        return array('server', 'username', 'password', 'database', 'maxlimit', 'timezone', 'encoding');
    }

    public function __wakeup() {
        MySQLFunction::connect();
    }

}

?>
