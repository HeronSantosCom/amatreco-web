<?php

include 'MySQLFunction.php';

class MySQLInteraction extends MySQLFunction {

    public function fields() {

        $table = null;
        if ($this->table) {
            foreach ($this->table as $obj) {
                if (isset($obj->name)) {
                    $table = MySQLFunction::eraser($obj->name);
                }
            }
        }

        $return = false;
        if ($table) {
            $query = mysql_list_fields($this->database, $table, $this->link);
            if ($query) {
                if (($fields = mysql_num_fields($query)) > 0) {
                    for ($x = 0; $x <= $fields - 1; $x++) {
                        $return[] = mysql_field_name($query, $x);
                        ;
                    }
                }
            }
        }
        return MySQLFunction::unbuffer($return);
    }

    public function select() {

        $table = null;
        if ($this->table) {
            foreach ($this->table as $obj) {
                if (isset($obj->name)) {
                    $_table[] = ($obj->database ? "{$obj->database}." : null) . $obj->name . ($obj->alias ? " `{$obj->alias}`" : null);
                }
            }
            $table = (isset($_table) ? "FROM " . join(", ", $_table) : null);
        }

        $column = "*";
        if ($this->column) {
            foreach ($this->column as $obj) {
                if (isset($obj->name)) {
                    $_column[] = ($obj->table ? "{$obj->table}." : null) . $obj->name . ($obj->alias ? " AS '{$obj->alias}'" : null);
                }
            }
            $column = (isset($_column) ? join(", ", $_column) : "*");
        }

        $where = null;
        if ($this->where) {
            foreach ($this->where as $index => $obj) {
                if (isset($obj->condition)) {
                    $_where[] = MySQLFunction::dowhere($index);
                }
            }
            $where = (isset($_where) ? "WHERE " . join(" ", $_where) : null);
        }

        $order = null;
        if ($this->order) {
            foreach ($this->order as $obj) {
                if (isset($obj->name)) {
                    $_order[] = ($obj->database ? "{$obj->database}." : null) . $obj->name . ($obj->order ? " {$obj->order}" : null);
                }
            }
            $order = (isset($_order) ? "ORDER BY " . join(", ", $_order) : null);
        }

        $limit = null;
        if (isset($this->limit->rows)) {
            $limit = "LIMIT " . ($this->limit->offset ? (string) $this->limit->offset . ", " : null) . (string) $this->limit->rows;
        }

        $return = false;
        if ($table) {
            $query = MySQLFunction::query("SELECT {$column} {$table} {$where} {$order} {$limit}");
            if ($query) {
                if ($query["rows"] <= $this->maxlimit) {
                    while ($fetch_array = mysql_fetch_array($query["result"], MYSQL_ASSOC)) {
                        $return[] = $fetch_array;
                    }
                } else {
                    ips_error("A consulta retornou mais de {$this->maxlimit} de registros.");
                }
            }
        }
        return MySQLFunction::unbuffer($return);
    }

    public function insert() {

        $table = null;
        if ($this->table) {
            foreach ($this->table as $obj) {
                if (isset($obj->name)) {
                    $_table[] = ($obj->database ? "{$obj->database}." : null) . $obj->name . ($obj->alias ? " `{$obj->alias}`" : null);
                }
            }
            $table = (isset($_table) ? "INTO " . join(", ", $_table) : null);
        }

        $column = null;
        if ($this->column) {
            foreach ($this->column as $obj) {
                if (isset($obj->name)) {
                    $_column[] = ($obj->table ? "{$obj->table}." : null) . $obj->name . ($obj->value ? " = {$obj->value}" : " = NULL");
                }
            }
            $column = (isset($_column) ? "SET " . join(", ", $_column) : null);
        }

        $return = false;
        if ($table and $column) {
            $query = MySQLFunction::query("INSERT {$table} {$column}");
            if ($query) {
                if ($query["result"]) {
                    $return = array($query["result"], $query["id"]);
                }
            }
        }
        return MySQLFunction::unbuffer($return);
    }

    public function update() {

        $table = null;
        if ($this->table) {
            foreach ($this->table as $obj) {
                if (isset($obj->name)) {
                    $_table[] = ($obj->database ? "{$obj->database}." : null) . $obj->name . ($obj->alias ? " `{$obj->alias}`" : null);
                }
            }
            $table = (isset($_table) ? join(", ", $_table) : null);
        }

        $column = null;
        if ($this->column) {
            foreach ($this->column as $obj) {
                if (isset($obj->name)) {
                    $_column[] = ($obj->table ? "{$obj->table}." : null) . $obj->name . ($obj->value ? " = {$obj->value}" : " = NULL");
                }
            }
            $column = (isset($_column) ? "SET " . join(", ", $_column) : null);
        }

        $where = null;
        if ($this->where) {
            foreach ($this->where as $index => $obj) {
                if (isset($obj->condition)) {
                    $_where[] = MySQLFunction::dowhere($index);
                }
            }
            $where = (isset($_where) ? "WHERE " . join(" AND ", $_where) : null);
        }

        $return = false;
        if ($table and $column) {
            $query = MySQLFunction::query("UPDATE {$table} {$column} {$where}");
            if ($query) {
                if ($query["result"]) {
                    $return = array($query["result"], $query["rows"]);
                }
            }
        }
        return MySQLFunction::unbuffer($return);
    }

    public function save() {

        foreach ($this->where as $index => $obj) {
            if (isset($obj->condition)) {
                return MySQLInteraction::update();
                break;
            }
        }
        return MySQLInteraction::insert();
    }

    public function delete() {

        $table = null;
        if ($this->table) {
            foreach ($this->table as $obj) {
                if (isset($obj->name)) {
                    $_table[] = ($obj->database ? "{$obj->database}." : null) . $obj->name . ($obj->alias ? " `{$obj->alias}`" : null);
                }
            }
            $table = (isset($_table) ? "FROM " . join(", ", $_table) : null);
        }

        $where = null;
        if ($this->where) {
            foreach ($this->where as $index => $obj) {
                if (isset($obj->condition)) {
                    $_where[] = MySQLFunction::dowhere($index);
                }
            }
            $where = (isset($_where) ? "WHERE " . join(" ", $_where) : null);
        }

        $return = false;
        if ($table) {
            $query = MySQLFunction::query("DELETE {$table} {$where}");
            if ($query) {
                if ($query["result"]) {
                    $return = array($query["result"], $query["rows"]);
                }
            }
        }
        return MySQLFunction::unbuffer($return);
    }

}

?>
