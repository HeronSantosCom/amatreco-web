<?php

class MySQLFormat {

    public function table($name, $database = false, $alias = false) {
        //set database if not defined
        if (!$database) {
            $database = $this->database;
        }
        //detected object of name
        if (is_object($name)) {
            $obj = MySQLFunction::discover($name);
            if ($obj) {
                $name = $obj;
                $database = false;
            }
        } else {
            $name = MySQLFunction::eraser($name, "`");
        }
        //declare objects
        $object = (object) null;
        $object->database = MySQLFunction::eraser($database, "`");
        $object->name = $name;
        $object->alias = MySQLFunction::eraser($alias);
        if ($this->return) {
            $this->return = false;
            $object->format = "table";
            return $object;
        }
        $this->table->{MySQLFunction::pointer("table")} = $object;
        return $this;
    }

    public function column($name, $value = false, $table = 1, $alias = false) {
        //set database if not defined
        $database = "`{$this->database}`";
        //set table of index defined
        if ($table) {
            if (isset($this->table->$table)) {
                $_alias = $this->table->$table->alias;
                $database = $this->table->$table->database;
                $table = $this->table->$table->name;
                if ($_alias) {
                    $database = false;
                    $table = "`{$_alias}`";
                }
            } else {
                ips_error("Tabela especificada não existe!");
            }
        }
        //detected object of name
        if (is_object($name)) {
            $obj = MySQLFunction::discover($name);
            if ($obj) {
                $name = $obj;
                $table = false;
                $value = false;
            }
        } else {
            if (preg_match("/(\(|\)|\*)/", $name)) {
                $database = false;
                $table = false;
                $alias = $value;
                $value = false;
            } else {
                if ($name != "*") {
                    $name = MySQLFunction::eraser($name, "`");
                }
            }
        }
        //detected object of value
        if (is_object($value)) {
            $obj = MySQLFunction::discover($value);
            if ($obj) {
                $value = $obj;
            }
        } else {
            if ($value and !preg_match("/(NOW)\(/", $value)) {
                $value = "\"" . mysql_real_escape_string($value, $this->link) . "\"";
            }
        }
        //declare objects
        $object = (object) null;
        $object->table = ($database ? "{$database}." : null) . $table;
        $object->name = $name;
        $object->alias = MySQLFunction::eraser($alias);
        $object->value = $value;
        if ($this->return) {
            $this->return = false;
            $object->format = "column";
            return $object;
        }
        $this->column->{MySQLFunction::pointer("column")} = $object;
        return $this;
    }

    public function where($condition, $of = false, $comparation = false, $glue = null, $index = false) {
        if ($index) {
            $args = func_get_args();
            $object = $this->where;
            if (isset($args[5])) {
                $object = $args[5];
            }
            $pointer = $index;
            if (isset($args[6])) {
                $pointer = $args[6];
            }
            if (strpos($index, ",")) {
                $index = explode(",", $index);
                $whereIndex = array_shift($index);
                $index = join(",", $index);
                $object = $object->$whereIndex->group;
                if (is_object($object)) {
                    return $this->where($condition, $of, $comparation, $glue, $index, $object, $pointer);
                }
            }
            if (isset($object->$index)) {
                $object->$index->group->{MySQLFunction::pointer("where-{$pointer}")} = $this->get->where($condition, $of, $comparation, $glue);
            }
            return false;
        }
        //detected object of condition
        if (is_object($condition)) {
            $obj = MySQLFunction::discover($condition);
            if ($obj) {
                $condition = $obj;
            }
        } else {
            $condition = MySQLFunction::eraser($condition, "`");
        }
        //detected object of comparation
        if (is_object($comparation)) {
            $obj = MySQLFunction::discover($comparation);
            if ($obj) {
                $comparation = $obj;
            }
        } else {
            if ($comparation) {
                if (preg_match("/(IN)/", $of)) {
                    $comparation = mysql_real_escape_string($comparation, $this->link);
                } else {
                    $comparation = "\"" . mysql_real_escape_string($comparation, $this->link) . "\"";
                }
            } else {
                $comparation = "NULL";
            }
        }
        //declare objects
        $object = (object) null;
        $object->condition = "{$condition} {$of} {$comparation}";
        $object->glue = strtoupper($glue);
        $object->group = false;
        if ($this->return) {
            $this->return = false;
            $object->format = "where";
            return $object;
        }
        $this->where->{MySQLFunction::pointer("where")} = $object;
        return $this;
    }

    public function order($name, $order = "ASC") {
        //set database if not defined
        $database = $this->database;
        //detected object of name
        if (is_object($name)) {
            $obj = MySQLFunction::discover($name);
            if ($obj) {
                $name = $obj;
                $database = false;
            }
        } else {
            $name = MySQLFunction::eraser($name, "`");
        }
        //declare objects
        $object = (object) null;
        $object->database = MySQLFunction::eraser($database, "`");
        $object->name = $name;
        $object->order = $order;
        if ($this->return) {
            $this->return = false;
            $object->format = "order";
            return $object;
        }
        $this->order->{MySQLFunction::pointer("order")} = $object;
        return $this;
    }

    public function limit($rows, $offset = false) {
        $object = (object) null;
        $object->rows = $rows;
        $object->offset = $offset;
        $this->limit = $object;
        return $this;
    }

}

?>
