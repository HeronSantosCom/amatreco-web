<?php

include 'MySQLFormat.php';

class MySQLFunction extends MySQLFormat {

    protected function connect() {
        $this->link = mysql_connect($this->server, $this->username, $this->password);
        if ($this->link) {
            if (!mysql_select_db($this->database, $this->link)) {
                ips_error("Não foi possível selecionar o banco de dados {$this->database}.");
            }
            if ($this->encoding) {
                if (mysql_client_encoding() != $this->encoding) {
                    mysql_query("SET NAMES {$this->encoding}", $this->link);
                }
            }
            if ($this->timezone) {
                mysql_query("SET SESSION time_zone = '{$this->timezone}'", $this->link);
            }
            return MySQLFunction::unbuffer(true);
        }
        ips_error("Não foi possível conectar ao servidor de banco de dados {$this->server}.");
    }

    protected function query($query) {
        //print "{$query}<br>\n";
        $result["result"] = mysql_query($query, $this->link);
        $result["rows"] = mysql_affected_rows($this->link);
        $result["id"] = mysql_insert_id($this->link);
        $result["error"] = mysql_error($this->link);
        if ($result["error"]) {
            $this->lasterror = $result["error"];
            ips_error($result["error"]);
        }
        return $result;
    }

    protected function unbuffer($return = false) {
        $this->table = (object) null;
        $this->column = (object) null;
        $this->where = (object) null;
        $this->join = (object) null;
        $this->group = (object) null;
        $this->order = (object) null;
        $this->limit = (object) null;
        $this->pointer = (object) null;
        return $return;
    }

    protected function pointer($object) {
        return $this->pointer->$object = (isset($this->pointer->$object) ? $this->pointer->$object + 1 : 1);
    }

    protected function eraser($value, $capsulate = false) {
        $value = trim(str_replace(array('"', "'", "`"), "", $value));
        if (strlen($value) > 0 and $capsulate) {
            $value = "{$capsulate}{$value}{$capsulate}";
        }
        return $value;
    }

    protected function dowhere($index, $object = false) {
        if (!$object) {
            $object = $this->where->$index;
        }
        if (is_object($object)) {
            if ($object->group) {
                foreach ($object->group as $index => $where) {
                    $object->condition .= " {$this->dowhere($index, $where)}";
                }
                $object->condition = "({$object->condition})";
            }
            return ($object->glue ? "{$object->glue} " : null) . $object->condition;
        }
        return false;
    }

    protected function discover($object) {
        if (is_object($object)) {
            $obj = $object;
            if (isset($obj->format)) {
                switch ($obj->format) {
                    case "function":
                        $object = $obj->name;
                        break;
                    case "column":
                        $object = ($obj->table ? "{$obj->table}." : null) . $obj->name;
                        break;
                    default :
                        ips_error("Tipo de objeto não permitido!");
                        break;
                }
            } else {
                ips_error("Tipo de condição não é válida!");
            }
            return $object;
        }
        return false;
    }

}

?>
